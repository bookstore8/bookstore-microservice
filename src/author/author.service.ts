import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateAuthorDto, UpdateAuthorDto } from './dto/author.dto';
import { Author } from './entities/author.entity';

@Injectable()
export class AuthorService {
  constructor(
    @InjectRepository(Author)
    private AuthorRepository: Repository<Author>,    
  ) {}

  async create(createAuthorData: CreateAuthorDto): Promise<object> {
    try {
      await this.AuthorRepository.insert(createAuthorData)
      return createAuthorData
    }
    catch (e) {
      console.log(`Не удалось создать автора ${e}`)
      throw new HttpException(
        'Не удалось создать автора',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );

    }
  }

  async addBooksForAuthor(booksAuthorData: {authorId: string, booksIds: Array<object>}): Promise<object> {
    try {
      await this.AuthorRepository.update({id: booksAuthorData.authorId}, {books: booksAuthorData.booksIds})
      return booksAuthorData
    }
    catch (e) {
      console.log(`Не удалось создать автора ${e}`)
      throw new HttpException(
        'Не удалось добавить книги автору',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );

    }
  }

  async findAll() {
    return await this.AuthorRepository.find()
  }

  async findOne(id: string) {
    console.log(id)
    return await this.AuthorRepository.findOneBy({
      id:id,
    })
  }

  async update(id: string, updateAuthorData: UpdateAuthorDto) {
    try {
      await this.AuthorRepository.update(id, updateAuthorData);
      return updateAuthorData

    } catch (e) {
      console.log('не удаолось обновить автора', e.stack || e);
      throw new HttpException(
        'не удаолось обновить автора',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }


  async changeRating(id: string, grade: number): Promise<number> {
    const author = this.findOne(id)
    const currentRating = (await author).rating
    const newRating = (currentRating + grade) / 2
    ;(await author).rating = newRating
    this.AuthorRepository.update({id: id}, await author)
    return newRating
  }
}
