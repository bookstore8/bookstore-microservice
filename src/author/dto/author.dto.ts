import {
    IsNumber,
    IsString,
    Length,
    IsPositive,
    Min,
    Max,
    IsDate

  } from 'class-validator';
import { PartialType } from '@nestjs/mapped-types';


export class BaseAuthorDto {
    @IsString()
    @Length(5, 50)
    firstName: string;
  
  
    @IsString()
    @Length(5, 100)
    lastName: string;


    @IsNumber()
    @IsPositive()
    @Min(15)
    @Max(150) 
    age: number
}


  
export class CreateAuthorDto extends PartialType(BaseAuthorDto){

}


export class ReadAuthorDto extends PartialType(BaseAuthorDto) {

    @IsString()
    id: string

    @IsNumber()
    @IsPositive()  
    @Min(1)
    @Max(1)
    like: number


    @IsNumber()
    @IsPositive()
    @Min(1)
    @Max(1)
    dislike: number


    @IsNumber()
    @Min(0)
    @Max(5)
    @IsNumber()
    rating: number

    
    @IsDate()
    createdAt: Date


    @IsDate()
    updatedAt: Date
}


export class UpdateAuthorDto extends PartialType(BaseAuthorDto) {

}
