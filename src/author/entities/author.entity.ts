import { Book } from "../../book/entities/book.entity";
import {
    Column,
    OneToMany,
    CreateDateColumn,
    UpdateDateColumn,
    Entity,
    PrimaryGeneratedColumn,
  } from 'typeorm';
  

@Entity()
export class Author {
    @PrimaryGeneratedColumn('uuid')
    id: string

    @Column({nullable: false})
    firstName: string

    @Column({nullable: false})
    lastName: string

    @Column('int', {nullable: false})
    age: number


    @Column('int', {default: 5})
    rating: number

    @Column()
    @CreateDateColumn()
    createdAt: Date

    @Column()
    @UpdateDateColumn()
    updatedAt: Date

    @OneToMany(() => Book, (book) => book.author)
    books: Book[]
}
