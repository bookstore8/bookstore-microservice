import { Controller} from '@nestjs/common';
import { MessagePattern, EventPattern } from '@nestjs/microservices';
import { AuthorService } from './author.service';

import { CreateAuthorDto, UpdateAuthorDto, } from './dto/author.dto';

import { Author } from './entities/author.entity';

@Controller('author')
export class AuthorController {
  constructor(private readonly authorService: AuthorService) {}

  @MessagePattern({ cmd: 'createAuthor' })
  async create(createAuthorData: CreateAuthorDto): Promise<CreateAuthorDto> {
    return this.authorService.create(createAuthorData);
  }

  @MessagePattern({ cmd: 'addBooksForAuthor' })
  async addBooksForAuthor(booksAuthorData: {authorId: string, booksIds: Array<object>}): Promise<CreateAuthorDto> {
    return this.authorService.addBooksForAuthor(booksAuthorData);
  }

  @MessagePattern({ cmd: 'findAllAuthors' })
  async findAll(): Promise<Author[]> {
    console.log('ALL AUTHORS MESSAGE')
    return this.authorService.findAll();
  }

  @MessagePattern({ cmd: 'findAuthor' })
  async findOne(id: string): Promise<Author> {
    return await this.authorService.findOne(id);
  }

  @MessagePattern({ cmd: 'updateAuthor' })
  async update(author: UpdateAuthorDto): Promise<UpdateAuthorDto>  {
    return await this.authorService.update(author['id'], author);
  }

  @EventPattern({ cmd: 'rateAuthor' })
  async rateAuthor(gradeData: {id: string, grade: number}) {
    await this.authorService.changeRating(gradeData.id, gradeData.grade).then(result =>{
      console.log(`Новая оценка автора: id - ${gradeData.id}, оценка: ${gradeData.grade}`)
    }).catch(error => {
      console.log(`Ошибка при оценке автора: ${error.stack || error}`)
    });
  }

}
