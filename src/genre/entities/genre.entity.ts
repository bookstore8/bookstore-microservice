import { Book } from 'src/book/entities/book.entity';
import {
    Column,
    Entity,
    OneToMany,
    PrimaryGeneratedColumn,
  } from 'typeorm';


@Entity()
export class Genre {
    @PrimaryGeneratedColumn('uuid')
    id: string
    
    @Column({nullable: false})
    name: string

    @OneToMany(() => Book, (book) => book.genre)
    books: Book[]
}
