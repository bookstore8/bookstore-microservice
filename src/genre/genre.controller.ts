import { Controller } from '@nestjs/common';
import { GenreService } from './genre.service';
import { UpdateGenreDto, CreateGenreDto } from './dto/genre.dto';
import { MessagePattern } from '@nestjs/microservices';

@Controller('genres')
export class GenreController {
  constructor(private readonly genreService: GenreService) {}

  @MessagePattern({ cmd: 'createGenre' })
  create(createGenreDto: CreateGenreDto) {
    return this.genreService.create(createGenreDto);
  }

  @MessagePattern({ cmd: 'findAllGenres' })
  findAll() {
    return this.genreService.findAll();
  }

  @MessagePattern({ cmd: 'findeOneGenre' })
  findOne(id: string) {
    return this.genreService.findOne(id);
  }

  @MessagePattern({ cmd: 'updateGenre' })
  update(updateGenreDara: UpdateGenreDto) {
    return this.genreService.update(updateGenreDara['id'], updateGenreDara);
  }

  @MessagePattern({ cmd: 'removeGenre' })
  remove(id: string) {
    return this.genreService.remove(id);
  }

  @MessagePattern({ cmd: 'restoreGenre' })
  restore(id: string) {
    return this.genreService.restore(id);
  }
}
