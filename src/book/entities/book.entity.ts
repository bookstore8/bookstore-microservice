import { Author } from 'src/author/entities/author.entity';
import { Genre } from 'src/genre/entities/genre.entity';
import {
    Column,
    ManyToOne,
    CreateDateColumn,
    UpdateDateColumn,
    Entity,
    PrimaryGeneratedColumn,
} from 'typeorm';
import {
    Length,
    IsDate,
} from "class-validator"


@Entity()
export class Book {
    @PrimaryGeneratedColumn('uuid')
    id: string
    
    @Column({nullable: false})
    @Length(5, 50)
    name: string


    @Column('int', {default: 5})
    rating: number

    @Column()
    @IsDate()
    @CreateDateColumn()
    createdAt: Date

    @Column()
    @IsDate()
    @UpdateDateColumn()
    updatedAt: Date

    @Column('boolean', {default: true})
    isActive: boolean

    @ManyToOne(() => Author, (author) => author.books)
    author: Author

    @ManyToOne(() => Genre, (genre) => genre.books)
    genre: Genre
}
