import { Controller } from '@nestjs/common';
import { BookService } from './book.service';
import { CreateBookDto, UpdateBookDto } from './dto/book.dto';
import { MessagePattern } from '@nestjs/microservices';


@Controller('book')
export class BookController {
  constructor(private readonly bookService: BookService) {}

  @MessagePattern({ cmd: 'createBook' })
  async create(createBookDto: CreateBookDto) {
    return this.bookService.create(createBookDto);
  }

  @MessagePattern({ cmd: 'findAllBooks' })
  async findAll() {
    return this.bookService.findAll();
  }

  @MessagePattern({ cmd: 'findOneBook' })
  async findOne(id: string) {
    return this.bookService.findOne(id);
  }

  @MessagePattern({ cmd: 'updateBook' })
  async update(updateBookDto: UpdateBookDto) {
    return this.bookService.update(updateBookDto['id'], updateBookDto);
  }

  @MessagePattern({ cmd: 'removeBook' })
  async remove(id: string) {
    return this.bookService.remove(id);
  }

  @MessagePattern({ cmd: 'restoreAuthor' })
  async restore(id: string) {
    return this.bookService.restore(id);
  }
}
