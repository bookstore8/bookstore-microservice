import {
    IsNumber,
    IsString,
    IsDate
    

  } from 'class-validator';


export class ReadBookDto {
    @IsString()
    name: string

    @IsNumber()
    rating: number

    @IsDate()
    createdAt: Date

    @IsDate()
    updatedAt: Date

    isActive: boolean
}

export class CreateBookDto {
    @IsString()
    name: string

    isActive: boolean

    genre: {id: string}
}


export class UpdateBookDto {
    @IsString()
    name: string

    isActive: boolean
}
