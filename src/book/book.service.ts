import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { HttpErrorByCode } from '@nestjs/common/utils/http-error-by-code.util';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateBookDto, UpdateBookDto } from './dto/book.dto';
import { Book } from './entities/book.entity';


@Injectable()
export class BookService {
  constructor(
    @InjectRepository(Book)
    private BookRepository: Repository<Book>,
  ) { }

  async create(createBookDto: CreateBookDto): Promise<object> {
    return await this.BookRepository.insert(createBookDto)
  }

  async findAll(): Promise<object> {
    return await this.BookRepository.find()
  }

  async findOne(id: string): Promise<object> {
    return await this.BookRepository.findOneBy({ id: id })
  }

  async update(id: string, updateBookDto: UpdateBookDto): Promise<object> {
    await this.BookRepository.update({ id: id }, updateBookDto)
    return updateBookDto
  }

  async remove(id: string): Promise<object> {
    try {
      const result = await this.BookRepository.softDelete({ id: id });
      if (result) {
        return {
          statusCode: 200,
          message: 'Book with id: ' + id + ' deleted',
        };
      }
    } catch (e) {
      throw new HttpErrorByCode[500]('Internal server error');
    }
  }

  async restore(id: string): Promise<object> {
    try {
      const result = await this.BookRepository.restore({ id: id });
      if (result) {
        return {
          statusCode: 200,
          message: 'Book with id: ' + id + ' restored',
        };
      }
    } catch (e) {
      throw new HttpErrorByCode[500]('Internal server error');
    }
  }
}
